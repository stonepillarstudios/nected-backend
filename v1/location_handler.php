<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 08 Mar 2018
 * Time: 4:44 PM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * input JSON:
 *  rauth: string
 *  coords: JSONObject:
 *      x: double
 *      y: double
 */

if (isset($input->coords) && isset($input->rauth)){

    $profileID = validate_rauth($input->rauth);
    if ($profileID == false){
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_RAUTH';
    }else {

        new_loc($input->profileID, $input->coords->x, $input->coords->y);

        $outputArr['success'] = true;

    }

}else{
    $outputArr['success'] = false;
    $outputArr['failMsg'] = 'profileID and/or coords not set';
}

echo json_encode($outputArr);
$db->close();