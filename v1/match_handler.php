<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 29 Jan 2018
 * Time: 1:11 PM
 */

require_once 'utils/funcs.php';

$outputArr = array();

$input = json_decode(file_get_contents("php://input"));

/**
 * input JSON:
 * rauth: string
 */

if (isset($input->rauth)){

    $profileID = validate_rauth($input->rauth);
    if ($profileID == false){
        $outputArr['success'] = false;
        $outputArr['failMsg'] = 'BAD_RAUTH';

    }else{
        $matches = get_matches($profileID);

        $outputArr['matches'] = $matches;
        $outputArr['success'] = true;

    }

}else{
    $outputArr['success'] = false;
    $outputArr['failMsg'] = 'BAD_RAUTH';
}

echo json_encode($outputArr);
$db->close();