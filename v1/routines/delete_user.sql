DROP PROCEDURE IF EXISTS delete_user;
CREATE PROCEDURE delete_user(IN pProfileID VARCHAR(255))
  BEGIN

    DELETE FROM user_settings WHERE profileID = pProfileID;
    DELETE FROM locations WHERE profileID = pProfileID;
    DELETE FROM ignores WHERE ignorer = pProfileID OR ignoree = pProfileID;
    DELETE FROM ratings WHERE rater = pProfileID OR ratee = pProfileID;
    DELETE FROM reports WHERE reporter = pProfileID OR reportee = pProfileID;
    DELETE FROM likes WHERE profileID = pProfileID;
    DELETE FROM users WHERE profileID = pProfileID;

  END;
