DROP FUNCTION IF EXISTS get_max_radius;
CREATE FUNCTION get_max_radius(pProfileID VARCHAR(255))
  RETURNS DOUBLE
  BEGIN

    DECLARE dist_index INTEGER DEFAULT
      get_setting(pProfileID, 'MAX_DISTANCE_INDEX');

    DECLARE min_dist INTEGER DEFAULT
      CASE
      WHEN dist_index = 0 THEN 10
      WHEN dist_index = 1 THEN 25
      WHEN dist_index = 2 THEN 250
      WHEN dist_index = 3 THEN 750
      WHEN dist_index = 4 THEN 41000
      END;

    RETURN min_dist;

  END;
