DROP FUNCTION IF EXISTS get_setting;
CREATE FUNCTION get_setting(reqPID VARCHAR(255), settKEY VARCHAR(32))
  RETURNS TINYTEXT
  BEGIN

    DECLARE val TINYTEXT DEFAULT "";

    SELECT setting_value INTO val FROM user_settings
    WHERE profileID = reqPID
          AND setting_key = settKEY
    LIMIT 1;

    RETURN val;
  END;
