DROP PROCEDURE IF EXISTS create_new_user;
CREATE PROCEDURE create_new_user(IN pProfileID VARCHAR(255), IN pFirstname TINYTEXT, IN pLastname TINYTEXT,
                                 IN pEmail     TINYTEXT, IN pDoB DATE, IN pGender CHAR)
  BEGIN

    DECLARE bDone BOOLEAN DEFAULT 0;

    DECLARE gKey VARCHAR(32);
    DECLARE gVal TINYTEXT;

    DECLARE age INTEGER DEFAULT
      FLOOR(DATEDIFF(NOW(), pDoB) / 365.25);

    DECLARE min_age INTEGER DEFAULT
      age - 7;

    DECLARE max_age INTEGER DEFAULT
      age + 7;

    DECLARE curs CURSOR FOR
      SELECT setting_key, default_value FROM default_settings;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = true;


    IF NOT user_exists(pProfileID) THEN

      INSERT INTO users (profileID, first_name, last_name, email, DoB, gender) VALUES (pProfileID, pFirstname, pLastname, pEmail, pDoB, pGender);


      OPEN curs;

      FETCH curs INTO gKey, gVal;

      WHILE NOT bDone DO

        INSERT INTO user_settings (profileID, setting_key, setting_value)
        VALUES (pProfileID, gKey, gVal);

        FETCH curs INTO gKey, gVal;

      END WHILE;

      CLOSE curs;


      IF min_age < 18 THEN
        SET min_age = 18;
      END IF;
      IF max_age > 80 THEN
        SET max_age = 80;
      END IF;

      UPDATE user_settings SET setting_value = CONCAT(min_age, '')
      WHERE profileID = pProfileID
            AND setting_key = 'MIN_AGE';

      UPDATE user_settings SET setting_value = CONCAT(max_age, '')
      WHERE profileID = pProfileID
            AND setting_key = 'MAX_AGE';

    END IF;

  END;
