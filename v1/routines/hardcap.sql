DROP FUNCTION IF EXISTS hardcap;
CREATE FUNCTION hardcap(x DOUBLE, cap DOUBLE)
  RETURNS DOUBLE
  BEGIN

    IF x > cap THEN
      RETURN cap;
    ELSE
      RETURN x;
    END IF;

  END;
