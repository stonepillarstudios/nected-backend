DROP FUNCTION IF EXISTS within_distance;
CREATE FUNCTION within_distance(matcherPID VARCHAR(255), matcheePID VARCHAR(255))
  RETURNS TINYINT(1)
  BEGIN

    DECLARE lCount INTEGER DEFAULT
      (SELECT COUNT(DISTINCT profileID)
       FROM locations
       WHERE profileID = matcherPID
             OR profileID = matcheePID);

    DECLARE matcherLoc POINT DEFAULT
      (SELECT coords
       FROM locations
       WHERE profileID = matcherPID
       ORDER BY update_timestamp DESC
       LIMIT 1);

    DECLARE matcheeLoc POINT DEFAULT
      (SELECT coords
       FROM locations
       WHERE profileID = matcheePID
       ORDER BY update_timestamp DESC
       LIMIT 1);

    DECLARE maxDistance DOUBLE DEFAULT
      get_max_radius(matcherPID);

    IF lCount != 2 THEN -- if loc of both users not avail
      RETURN 0;
    ELSE

      RETURN (
               6371 *
               acos(
                   cos( radians( Y(matcherLoc) ) ) *
                   cos( radians( Y(matcheeLoc) ) ) *
                   cos(
                       radians( X(matcheeLoc) ) - radians( X(matcherLoc) )
                   ) +
                   sin(radians(Y(matcherLoc))) *
                   sin(radians(Y(matcheeLoc)))
               )
             ) <= maxDistance;

    END IF;
  END;
