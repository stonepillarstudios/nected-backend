DROP FUNCTION IF EXISTS matchability;
CREATE FUNCTION matchability(matcherPID VARCHAR(255), matcheePID VARCHAR(255), like_match_count INTEGER)
  RETURNS DOUBLE
  BEGIN -- calculates fudged match quality % using user age & like_matches

    DECLARE fMatchability, ageRelation, commonPerc DOUBLE DEFAULT 0.0;
    DECLARE matcherAge, matcheeAge, ageDiff INTEGER DEFAULT 0;

    SELECT FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) INTO matcherAge
    FROM users WHERE profileID = matcherPID;

    SELECT FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) INTO matcheeAge
    FROM users WHERE profileID = matcheePID;

    SET commonPerc = hardcap(like_match_count/get_total_likes(matcherPID) * 10, 1.0);
    -- realistically speaking, 10% likes matches is the max

    SET ageDiff = hardcap( ABS(matcherAge-matcheeAge) , 72);
    SET matcherAge = hardcap(matcherAge, 80);

    SET ageRelation = 60 * POW(2, 18/72) * POW(2, 0-(matcherAge/72));
    -- when age 18, ageDiff weighs 60%
    -- when age 80, ageDiff weighs 30%

    SET fMatchability = (commonPerc * (100-ageRelation)) + (((72-ageDiff)/72) * ageRelation);

    IF fMatchability > 98.0 THEN
      SET fMatchability = 98.0;
    ELSEIF fMatchability < 0.0 THEN
      SET fMatchability = 0;
    END IF;

    RETURN fMatchability;

  END;
