<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 28 Apr 2018
 * Time: 11:29 AM
 */

require_once '../utils/db_dev.php';
require_once '../utils/funcs.php';

function random_name(){

    $name = chr(rand(ord('A'), ord('Z')));

    for ($i = 0; $i < rand(1, 10); $i++){
        $name .= chr(rand(ord('a'), ord('z')));
    }

    return $name;

}

if ($_GET['admin_auth'] != 'Vu2xJCGzLAy4'){
    echo '<h1>ERROR: bad auth</h1>';
    exit();
}

if (!isset($_GET['start']) || !isset($_GET['size'])){
    echo '<h1>ERROR: bad GET params</h1>';
    exit();
}

echo '<h1>new users:</h1>';
echo '<ul>';

$start = $_GET['start'];
$req_num = $_GET['size'];

$stmt = $db->prepare('CALL create_new_user(?,?,?,?,?,?)');
$stmt2 = $db->prepare('UPDATE users SET messengerUsername = ? WHERE profileID = ?');
$stmt3 = $db->prepare('INSERT INTO locations(profileID, coords) VALUES (?, POINT(?,?))');

$new_users = 0;

for ($i = $start; $i < $start+$req_num; $i++){

    $fname = random_name();
    $lname = random_name();
    $email = $fname.'@example.com';
    $dob = rand(1900, 1998).'-'.rand(1, 12).'-'.rand(1,28);
    $g = rand(0,1) == 0 ? 'M' : 'F';

    $stmt->bind_param('ssssss', $i, $fname, $lname,
        $email, $dob, $g);
    $stmt->execute();

    $stmt2->bind_param('ss', $fname, $i);
    $stmt2->execute();

    $long = rand(-179999,179999)/1000;
    $lat = rand(-89999,89999)/1000;

    $stmt3->bind_param('sdd', $i, $long, $lat);
    $stmt3->execute();

    set_user_registered($i, true);

    echo '<li>'.$i.' - '.$email.'</li>';
    $new_users++;

}

$stmt->close();
$stmt2->close();
$stmt3->close();
$db->close();

echo '</ul><p>DONE. Added ' . $new_users . ' users</p>';