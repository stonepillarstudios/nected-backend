<?php
/**
 * Created by PhpStorm.
 * User: koell
 * Date: 28 Apr 2018
 * Time: 1:40 PM
 */

require_once '../utils/db_dev.php';
require_once '../utils/funcs.php';

if ($_GET['admin_auth'] != 'Vu2xJCGzLAy4'){
    echo '<h1>ERROR: bad auth</h1>';
    exit();
}

echo '<h1>Deleted users:</h1><ul>';

$users_q = $db->query('SELECT profileID FROM users');

$users_del = 0;

while (($users_r = $users_q->fetch_assoc()) != null){

    $db->query('CALL delete_user('.$users_r['profileID'].')');

    $users_del++;

    echo '<li>'.$users_r['profileID'].'</li>';

}

echo '</ul><p>Done. '.$users_del.' users deleted</p>';
