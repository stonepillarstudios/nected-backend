DROP PROCEDURE get_matches;
CREATE PROCEDURE get_matches(IN reqPID VARCHAR(32))
  BEGIN

    SELECT DISTINCT profileID, first_name, last_name,
      FLOOR(DATEDIFF(NOW(), DoB) / 365.25) AS age,
      matchability(reqPID, profileID) AS match_quality,
      messengerUsername

    FROM users

    WHERE profileID <> reqPID

    AND is_matchable(reqPID, profileID)

    AND is_matchable(profileID, reqPID)

    ORDER BY match_quality DESC;

END;
