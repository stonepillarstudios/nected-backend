DROP FUNCTION is_matchable;
CREATE FUNCTION is_matchable(matcherPID VARCHAR(255), matcheePID VARCHAR(255))
  RETURNS TINYINT(1)
  BEGIN

    DECLARE pCount INTEGER DEFAULT 0;

    DECLARE min_quality INTEGER DEFAULT
      get_setting(matcherPID, 'MATCH_QUALITY');

    DECLARE min_age INTEGER DEFAULT
      get_setting(matcherPID, 'MIN_AGE');

    DECLARE max_age INTEGER DEFAULT
      get_setting(matcherPID, 'MAX_AGE');

    DECLARE max_radius DOUBLE DEFAULT
      get_max_radius(matcherPID);

    IF max_age >= 80 THEN
      SET max_age = 9999;
    END IF;


    SELECT COUNT(profileID) INTO pCount

    FROM users

    WHERE users.profileID NOT IN (

      SELECT ignoree FROM ignores

      WHERE ignorer = matcherPID

    )

    AND users.profileID = matcheePID

    AND matchability(matcherPID, matcheePID) >= min_quality

    AND (FLOOR(DATEDIFF(NOW(), users.DoB) / 365.25) BETWEEN min_age AND max_age)

    AND within_distance(matcherPID, matcheePID)

    AND gender_matches(matcherPID, matcheePID)

    AND registered = TRUE;


    RETURN pCount;
  END;
