DROP FUNCTION IF EXISTS get_total_likes;
CREATE FUNCTION get_total_likes(reqPID VARCHAR(255))
  RETURNS INT
  BEGIN

    DECLARE total_pages INTEGER DEFAULT 0;

    SELECT COUNT(pageID) INTO total_pages FROM likes
    WHERE profileID = reqPID;

    RETURN total_pages;
  END;
