DROP PROCEDURE IF EXISTS add_ignore_couple;
CREATE PROCEDURE add_ignore_couple(IN pIgnorer VARCHAR(255), IN pIgnoree VARCHAR(255))
  BEGIN

    INSERT INTO ignores (ignorer, ignoree)
    VALUES (pIgnorer, pIgnoree), (pIgnoree, pIgnorer);

  END;
